class CreateRents < ActiveRecord::Migration[5.1]
  def change
    create_table :rents do |t|
      t.string :name
      t.decimal :total
      t.belongs_to :user, foreign_key: true

      t.timestamps
    end
  end
end
