class ItemController < ApplicationController

  def show
  end

  def destroy
    @item = Item.find(params[:id])
    @item.destroy
  end

end
