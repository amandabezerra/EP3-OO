class Ability  
  include CanCan::Ability  
  def initialize(user)  
    if user.present?  
      if user.role.name == 'admin'  
        can :manage, :all  
      else    
        can :create , Rent  
        can :read, :all  
      end  
    else  
      can :read, :all  
    end  
  end  
 end