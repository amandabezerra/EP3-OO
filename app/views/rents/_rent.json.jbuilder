json.extract! rent, :id, :name, :total, :user_id, :created_at, :updated_at
json.url rent_url(rent, format: :json)
