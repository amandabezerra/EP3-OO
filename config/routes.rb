Rails.application.routes.draw do
  resources :rents
  resources :products

  devise_for :users
  
  devise_scope :user do
  	root to: 'devise/sessions#new'  
  	#resources :users
  	get '/users/list', to: 'users#index', as: 'users/list'
    get 'item' => 'items#show', via: [:get, :post]
   resource :items


    resources :users
  end

  resources :activities

  match 'home' => 'home#index', via: 'get'
end
